#include <QCompleter>
#include <QStandardItemModel>

#include "test_qcompleter.h"
#include "ui_test_qcompleter.h"

TestQCompleter::TestQCompleter(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TestQCompleter)
{
    ui->setupUi(this);
    // runQCompleter();
    runModelBasedQCompleter();
}

TestQCompleter::~TestQCompleter()
{
    delete ui;
}

void TestQCompleter::runModelBasedQCompleter()
{
    // create model
    QStandardItemModel *model = new QStandardItemModel();
    QStringList list;
    list << "Ab" << "Aa" << "Ac" << "D qc" << "D qa" << "D qb";
    for (int i = 0; i < list.length(); i++)
    {
        QStandardItem *item = new QStandardItem();
        item->setText( list.at( i ) );
        item->setData( "real one, two or three is inserted here" );
        model->appendRow( item );
    }

    QLineEdit* edit = ui->lineEdit;
    edit->setWindowTitle("QLineEdit Auto Complete");

    QCompleter* completer = new QCompleter();
    
    //add model
    completer->setModel(model);

    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setFilterMode(Qt::MatchContains);

    edit->setCompleter(completer);
}

void TestQCompleter::runQCompleter()
{
    QLineEdit* edit = ui->lineEdit;
    edit->setWindowTitle("QLineEdit Auto Complete");

    QStringList* list = new QStringList();

    *list   << "Daniel Müller"
            << "Max Meier"
            << "Adam & Eva"
            << "Eva";

    QCompleter* completer = new QCompleter(*list);
    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setFilterMode(Qt::MatchContains);
    
    edit->setCompleter(completer);
    
}

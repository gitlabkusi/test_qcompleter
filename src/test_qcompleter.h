#ifndef TEST_QCOMPLETER_H
#define TEST_QCOMPLETER_H

#include <QMainWindow>

namespace Ui {
class TestQCompleter;
}

class TestQCompleter : public QMainWindow
{
    Q_OBJECT

public:
    explicit TestQCompleter(QWidget *parent = 0);
    ~TestQCompleter();

    void runQCompleter();
    void runModelBasedQCompleter();

private:
    Ui::TestQCompleter *ui;
};

#endif // TEST_QCOMPLETER_H

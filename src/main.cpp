#include "test_qcompleter.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    TestQCompleter w;
    w.show();

    return app.exec();
}

